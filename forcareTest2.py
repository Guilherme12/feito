from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located


# url = str(input("Digite uma url para pesquisa: "))


driver = webdriver.Chrome(executable_path=r'feito/chromedriver.exe')


driver.get('https://www.google.com')


# utilizei a tag input pois o name="q" não funciona
elemento = driver.find_element_by_tag_name('input')


elemento.send_keys("test")


elemento.send_keys(Keys.RETURN)

wait = WebDriverWait(driver, 10)


titulo1 = wait.until(presence_of_element_located(
    (By.XPATH, '//*[@id="rso"]/div[1]/div/div/div[1]/a/h3')))


titulo2 = wait.until(presence_of_element_located(
    (By.XPATH, '//*[@id="rso"]/div[2]/div/div/div[1]/a/h3')))


print("Titulo 1: ", titulo1.text)
print("Titulo 2: ", titulo2.text)
