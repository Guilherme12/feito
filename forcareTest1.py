
def moeda(preco=0, moeda='R$'):
    return f'{moeda}{preco:.2f}'.replace('.', ',')


valor_conta = float(input("Valor da refeição: "))
servico = int(input("Porcentagem serviço: "))

valor_servico = int(valor_conta * servico) / 100
total_soma = (valor_servico + valor_conta)

print(f"Valor da conta: {moeda(valor_conta)}")

print(f"valor do serviço: {moeda(valor_servico)}")

print(f"Valor da soma: {moeda(total_soma)}")
