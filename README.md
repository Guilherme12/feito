**Desafio 2**

Como baixar e instalar as features necessarias para utilizar o codigo do desafio 2.

---
## Como instalar o python fora de um ambiente virtual

1. Baixer e instale o python na sua maquina segue link para download - https://www.python.org/downloads/
2. Após concluir a instalação abra o CMD e instale o SELENIUM "pip install selenium"

Pronto selenium e o python já estão instalados na sua maquina

---
## Como rodar a aplicação pelo CMD

1. Abra o CMD "bandeira do windows + R"
2. Vá até a pasta onde se encontram os arquivos pelo CMD
3. Pelo CMD escreva Python forcareTest2.py e precione a tecla ENTRE DO TECLADO.


Passo 3 a sentença python precisa estar em maiuscula "P"ython. 



---
## Baixar e Instalar CROMEDRIVER 

Baixar o CROMEDRIVER verificando a versão do Chrome

Link: https://chromedriver.chromium.org/downloads

1. Verificar a versão do navegador (Chrome)
	1.1  Ajuda -> Sobre Google Chrome 
	1.2  Pagina /Help Verificar a versão do que está sendo utilizada do Chrome geralemtne são (89, 90, 91 e 92).
2. Após descobrir a versão do chrome basta ir a pasta ISO CROMEDRIVER onde pré-separei algumas versões mais atuais do CROMEDRIVER.
	2.1 Para extrair os arquivos do winrar basta apenas clicar com o botão direito do mause e ir em "Extrair aki".
3. Coloque o ChromeDriver.exe dentro da pasta do projeto para que possa funcionar corretamente.

